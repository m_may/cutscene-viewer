#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <sys/errno.h>
#include <err.h>

#define M_T_INDEX 0x03FF
#define M_T_PALETTE 0x1C00
#define M_T_PRIORITY 0x2000
#define M_T_HFLIP 0x4000
#define M_T_VFLIP 0x8000

#define S_T_INDEX 0
#define S_T_PALETTE 10
#define S_T_PRIORITY 13
#define S_T_HFLIP 14
#define S_T_VFLIP 15

struct tile {
	Uint16 index:10;
	Uint16 palette:3;
	Uint16 priority:1;
	Uint16 hflip:1;
	Uint16 vflip:1;
} tilemap[0x400];

struct appWindow {
	struct tile tilemap[0x400];
	unsigned char gfx[0x400][8][8];
	Uint16 gfxPaletted[8][0x400][8][8];
	Uint16 pal[8][16];
	SDL_Texture * tex[8];
	SDL_Window * win;
	SDL_Renderer * rend;
};
struct appWindow defaultAppWindow;

void draw(struct appWindow * w) {
	for(int i = 0; i < 0x20; ++i) {
		for(int j = 0; j < 0x20; ++j) {
			struct tile * tile = &w->tilemap[i*0x20+j];
			SDL_Rect src_rect = {.x = 0, .y = tile->index*8, .w = 8, .h = 8};
			SDL_Rect dest_rect = {.x = j*8, .y = i*8, .w = 8, .h = 8};
			SDL_RenderCopyEx(w->rend, w->tex[tile->palette], &src_rect, &dest_rect, 0, NULL,
				(tile->hflip*SDL_FLIP_HORIZONTAL)|(tile->vflip*SDL_FLIP_VERTICAL));
		}
	}
}

int initAppWindow(struct appWindow * w, int pos) {
	w->win = SDL_CreateWindow("viewer", pos, pos, 256, 256, 0);
	if(w->win == NULL) {
		printf("%s\n", SDL_GetError());
		goto initAppWindow_cleanup1;
	}

	w->rend = SDL_CreateRenderer(w->win, -1, SDL_RENDERER_PRESENTVSYNC);
	if(w->rend == NULL) {
		printf("%s\n", SDL_GetError());
		goto initAppWindow_cleanup2;
	}

	for(int i = 0; i < 8; ++i) {
		w->tex[i] = SDL_CreateTexture(w->rend, SDL_PIXELFORMAT_BGR555, SDL_TEXTUREACCESS_STATIC, 8, 8*0x400);
		if(w->tex[i] == NULL) {
			printf("%s\n", SDL_GetError());
			goto initAppWindow_cleanup3;
		}
	}

	return 0;

initAppWindow_cleanup3:
	SDL_DestroyRenderer(w->rend);

initAppWindow_cleanup2:
	SDL_DestroyWindow(w->win);

initAppWindow_cleanup1:
	free(w);
	return -1;
}

void destroyAppWindow(struct appWindow * w) {
	SDL_DestroyRenderer(w->rend);
	SDL_DestroyWindow(w->win);
}

int initSDL() {
	if(SDL_Init(SDL_INIT_VIDEO) != 0) return -1;
	atexit(SDL_Quit);
	return 0;
}

bool quit;

void mainSDL() {
	SDL_Event e;
	while(SDL_PollEvent(&e)) {
		switch(e.type) {
		case SDL_QUIT:
			quit = true;
			break;
		case SDL_WINDOWEVENT:
			if(e.window.event == SDL_WINDOWEVENT_CLOSE) {
				quit = true;
			}
			break;
		}
	}
	{
		struct appWindow * w = &defaultAppWindow;
		SDL_RenderClear(w->rend);
		draw(w);
		SDL_RenderPresent(w->rend);
	}
}

int loadFile(char * path, unsigned char * buffer, size_t size, size_t fileOffset) {
	FILE * fp = fopen(path, "rb");
	if(fp == NULL)
		goto loadFile_error_return;

	if(fseek(fp, fileOffset, SEEK_SET) != 0)
		goto loadFile_error_cleanup;

	fread(buffer, 1, size, fp);
	if(ferror(fp) != 0)
		goto loadFile_error_cleanup;

	fclose(fp);
	return 0;

loadFile_error_cleanup:
	fclose(fp);

loadFile_error_return:
	warn("%s", path);
	return -1;
}

void processTilemap(struct tile tilemap[0x400], unsigned char buffer[0x800]) {
	for(int i = 0; i < 0x400; ++i) {
		Uint16 tile = buffer[i*2]+(buffer[i*2+1]<<8);
		tilemap[i].index = (tile & M_T_INDEX) >> S_T_INDEX;
		tilemap[i].palette = (tile & M_T_PALETTE) >> S_T_PALETTE;
		tilemap[i].priority = (tile & M_T_PRIORITY) >> S_T_PRIORITY;
		tilemap[i].hflip = (tile & M_T_HFLIP) >> S_T_HFLIP;
		tilemap[i].vflip = (tile & M_T_VFLIP) >> S_T_VFLIP;
	}
}

void processGfx(unsigned char gfx[0x400][8][8], unsigned char buffer[][32], int charnum, int offset) {
	if(charnum > 0x400) {
		warnx("Number of GFX characters above 0x400. Clamping to 0x400.");
		charnum = 0x400;
	}
	for(int i = 0; i < charnum; ++i) {
		for(int j = 0; j < 8; ++j) {
			unsigned char bitplane[4] = {
				buffer[i][j*2],
				buffer[i][j*2+1],
				buffer[i][j*2+16],
				buffer[i][j*2+17]
			};
			for(int k = 0; k < 8; ++k) {
				for(int l = 0; l < 4; ++l)
					gfx[(offset+i)%0x400][j][k] += ((bitplane[l]>>(7-k))&1)<<l;
			}
		}
	}
}

void paletteGfx(Uint16 gfxPaletted[8][0x400][8][8], unsigned char gfx[0x400][8][8], Uint16 pal[8][16]) {
	for(int palnum = 0; palnum < 8; ++palnum) {
		for(int i = 0; i < 0x400; ++i) {
			for(int j = 0; j < 8; ++j) {
				for(int k = 0; k < 8; ++k) {
					gfxPaletted[palnum][i][j][k] = pal[palnum][gfx[i][j][k]];
				}
			}
		}
	}
}

void loadTilemap(struct appWindow * w, char * path, int fileOffset) {
	int size = 0x800;
	unsigned char * rawTilemap = malloc(size);
	if(rawTilemap == NULL)
		warn("tilemap");
	else if(loadFile(path, rawTilemap, size, fileOffset) == 0)
		processTilemap(w->tilemap, rawTilemap);

	free(rawTilemap);
}

void loadGfx(struct appWindow * w, char * path, int fileOffset, int charnum, int offset) {
	int size = charnum*32;
	unsigned char (* rawGfx)[32] = malloc(size);
	if(rawGfx == NULL)
		warn("gfx");
	else if(loadFile(path, (unsigned char *) rawGfx, size, fileOffset) == 0)
		processGfx(w->gfx, rawGfx, charnum, offset);

	free(rawGfx);
}

void loadPalette16(struct appWindow * w, char * path, int fileOffset) {
	int size = 8*16*2;
	unsigned char (* pal16)[2] = malloc(size);
	if(pal16 == NULL)
		warn("pal16");
	else if(loadFile(path, (unsigned char *) pal16, size, fileOffset) == 0) {
		for(int i = 0; i < 8*16; ++i)
			w->pal[i/16][i%16] = pal16[i][0] + (pal16[i][1]<<8);
	}

	free(pal16);
}

void loadPalette24(struct appWindow * w, char * path, int fileOffset) {
	int size = 8*18*3;
	unsigned char (* pal24)[3] = malloc(size);
	if(pal24 == NULL)
		warn("pal24");
	else if(loadFile(path, (unsigned char *) pal24, size, fileOffset) == 0) {
		for(int i = 0; i < 8*16; ++i)
			w->pal[i/16][i%16] = (pal24[i][0]>>3) + ((pal24[i][1]>>3)<<5) + ((pal24[i][2]>>3)<<10);
	}

	free(pal24);
}

int main(int argc, char * argv[]) {
	errno = 0;

	if(initSDL() != 0) {
		printf("%s\n", SDL_GetError());
		return 1;
	}

	struct appWindow * w = &defaultAppWindow;
	if(initAppWindow(w, SDL_WINDOWPOS_UNDEFINED) != 0)
		return 2;

	bool defaultPalette = true;

	int i = 1;
	while(i < argc) {
		enum {
			CMD_UNKNOWN,
			CMD_TILEMAP,
			CMD_GFX,
			CMD_PALETTE
		} cmd = CMD_UNKNOWN;

		if(strcmp(argv[i], "tilemap") == 0)
			cmd = CMD_TILEMAP;
		else if(strcmp(argv[i], "gfx") == 0 || strcmp(argv[i], "graphics") == 0)
			cmd = CMD_GFX;
		else if(strcmp(argv[i], "pal") == 0 || strcmp(argv[i], "palette") == 0)
			cmd = CMD_PALETTE;

		i += 1;

		switch(cmd) {
		case CMD_TILEMAP:
		{
			char * path = NULL;
			size_t fileOffset = 0;
			while(i < argc) {
				if(strcmp(argv[i], "-path") == 0 || strcmp(argv[i], "-file") == 0 || strcmp(argv[i], "-filepath") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					path = argv[i+1];
					i += 2;
				} else if(strcmp(argv[i], "-file-offset") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					fileOffset = strtol(argv[i+1], NULL, 0);
					i += 2;
				} else {
					break;
				}
			}
			if(path != NULL)
				loadTilemap(w, path, fileOffset);
			else
				warnx("%s: Filepath missing.", "Tilemap");
			break;
		}
		case CMD_GFX:
		{
			char * path = NULL;
			size_t fileOffset = 0;
			int charnum = 0x400;
			int tileOffset = 0;
			while(i < argc) {
				if(strcmp(argv[i], "-path") == 0 || strcmp(argv[i], "-file") == 0 || strcmp(argv[i], "-filepath") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					path = argv[i+1];
					i += 2;
				} else if(strcmp(argv[i], "-file-offset") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					fileOffset = strtol(argv[i+1], NULL, 0);
					i += 2;
				} else if(strcmp(argv[i], "-num") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					charnum = strtol(argv[i+1], NULL, 0);
					i += 2;
				} else if(strcmp(argv[i], "-tile-offset") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					tileOffset = strtol(argv[i+1], NULL, 0);
					i += 2;
				} else {
					break;
				}
			}
			if(path != NULL)
				loadGfx(w, path, fileOffset, charnum, tileOffset);
			else
				warnx("%s: Filepath missing.", "Gfx");
			break;
		}
		case CMD_PALETTE:
		{
			char * path = NULL;
			size_t fileOffset = 0;
			int palFormat = 0;
			while(i < argc) {
				if(strcmp(argv[i], "-path") == 0 || strcmp(argv[i], "-file") == 0 || strcmp(argv[i], "-filepath") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					path = argv[i+1];
					i += 2;
				} else if(strcmp(argv[i], "-file-offset") == 0) {
					if(i+1 >= argc) {
						warnx("Missing argument for %s.", argv[i]);
						break;
					}
					fileOffset = strtol(argv[i+1], NULL, 0);
					i += 2;
				} else if(strcmp(argv[i], "-rgb24") == 0) {
					palFormat = 1;
					i += 1;
				} else {
					break;
				}
			}
			defaultPalette = false;
			if(path != NULL) {
				if(palFormat == 1)
					loadPalette24(w, path, fileOffset);
				else
					loadPalette16(w, path, fileOffset);
			} else
				warnx("%s: Filepath missing.", "Palette");
			break;
		}
		default:
			warnx("%s: Unknown command.", argv[i-1]);
		}
	}

	if(defaultPalette) {
		for(int i = 0; i < 16; ++i) {
			int c = i*2+1;
			unsigned char t[8][3] = {
				{c, c, c},
				{c, 0, 0},
				{0, c, 0},
				{0, 0, c},
				{c, c, 0},
				{c, 0, c},
				{0, c, c},
				{c, c/2, c}
			};
			for(int j = 0; j < 8; ++j)
				w->pal[j][i] = t[j][0] + (t[j][1]<<5) + (t[j][2]<<10);
		}
	}

	paletteGfx(w->gfxPaletted, w->gfx, w->pal);

	for(int i = 0; i < 8; ++i)
		SDL_UpdateTexture(w->tex[i], NULL, w->gfxPaletted[i], 16);

	while(!quit) {
		mainSDL();
	}
	destroyAppWindow(w);
	return 0;
}
